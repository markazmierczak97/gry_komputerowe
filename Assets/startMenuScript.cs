using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class startMenuScript : MonoBehaviour
{
    public void startGame(int level){
    	SceneManager.LoadScene(level);
    }

    public void returnMenu(){
    	SceneManager.LoadScene(0);
    }

    public void quitGame(){
    	Application.Quit();
    }
}
