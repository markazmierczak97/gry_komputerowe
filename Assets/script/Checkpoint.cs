using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public bool isUse;
    public GameObject ellen;
    public GameObject startPoint;

    // Start is called before the first frame update
    void Start()
    {
        isUse = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Ellen" && isUse == false)
        {
            isUse = true;

            float number = Random.Range(0, 100);

            if (number <= 50)
            {
                Debug.Log("Powrt do startu, " + number);
                ellen.transform.position = startPoint.transform.position;
            }
            else
            {
                Debug.Log("Checkpoint ustawiony, " + number);
                startPoint.transform.position = ellen.transform.position;
            }
        }
    }
}